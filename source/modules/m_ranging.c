#include "sdk_config.h"
#include "ifs_tof.h"
#include "m_ranging.h"
#include "ble_dists.h"
#define  NRF_LOG_MODULE_NAME "m_ranging     "
#include "nrf_log.h"
#include "macros_common.h"

static ble_dists_t m_dists;
static ifs_tof_t m_ifs_tof[NRF_SDH_BLE_CENTRAL_LINK_COUNT];

/**@brief Evt dispatcher for ifs_tof module*/
void ifs_tof_evt_handler(ifs_tof_evt_t * p_evt) {

	switch (p_evt->evt_type) {
	case IFS_TOF_EVT_DISTANCE_READY: {
		int16_t d = p_evt->p_ctx->last_d_estimation*10; //fake_d++;//

		NRF_LOG_INFO("sending distance to peer with conn_handle %d. d = %d\r\n",p_evt->p_ctx->conn_handle,d);
		ble_dists_estimated_distance_send(&m_dists, p_evt->p_ctx->conn_handle,d); //send estimation to ble peer
	}
		break;
	default: {
		//no implementation needed
	}
		break;
	}
}

/**@brief Function for passing the BLE event to the Thingy Sound service.
 *
 * @details This callback function will be called from the BLE handling module.
 *
 * @param[in] p_ble_evt    Pointer to the BLE event.
 */
static void ranging_on_ble_evt(ble_evt_t * p_ble_evt)
{
    //uint32_t err_code;
	ifs_tof_on_ble_evt(p_ble_evt);
    ble_dists_on_ble_evt(&m_dists, p_ble_evt);

    ble_gap_evt_t const * p_gap_evt = &p_ble_evt->evt.gap_evt;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_ADV_REPORT:
        	//managed into m_ble.c
            break;

        case BLE_GAP_EVT_CONNECTED:

            break;

        case BLE_GAP_EVT_DISCONNECTED:
        	NRF_LOG_INFO("Unregistering connection handle %d with ifs_tof module.\r\n",p_gap_evt->conn_handle);
        	ifs_tof_unregister_conn_handle(p_gap_evt->conn_handle);
            break;
        default:
        	break;
    }
}

static void ble_dists_event_handler(ble_dists_t * p_dists, ble_dists_evt_t * p_evt) {

    uint32_t err_code;
	switch (p_evt->evt_type) {
	case BLE_DISTS_EVT_NOTIFICATION_ENABLED:
		NRF_LOG_INFO("registering connection handle %d with ifs_tof module.\r\n",p_evt->conn_handle);
		err_code = ifs_tof_register_conn_handle(p_evt->conn_handle, ifs_tof_evt_handler);
	    if (err_code != NRF_SUCCESS)
	    {
	        NRF_LOG_ERROR("Cannot register - err_code = %d\r\n", err_code);
	    }
		break;
	case BLE_DISTS_EVT_NOTIFICATION_DISABLED:
		NRF_LOG_INFO("Unregistering connection handle %d with ifs_tof module.\r\n",p_evt->conn_handle);
		err_code = ifs_tof_unregister_conn_handle(p_evt->conn_handle);
	    if (err_code != NRF_SUCCESS)
	    {
	        NRF_LOG_ERROR("Cannot unregister - err_code = %d\r\n", err_code);
	    }
		break;
	default:
		break;
	}
}

/**@brief Function for initializing the Thingy Sound Service.
 *
 * @details This callback function will be called from the ble handling module to initialize the Thingy Sound service.
 *
 * @retval NRF_SUCCESS If initialization was successful.
 */
static uint32_t ranging_service_init(bool major_minor_fw_ver_changed)
{
    uint32_t              err_code;
    ble_dist_init_t 	  dists_init;

    memset(&dists_init, 0, sizeof(dists_init));

    dists_init.evt_handler                 = ble_dists_event_handler;

    // Here the sec level for the Heart Rate Service can be changed/increased.
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dists_init.dist_range_attr_md.cccd_write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dists_init.dist_range_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&dists_init.dist_range_attr_md.write_perm);

    NRF_LOG_INFO("ranging_service_init: ranging_service_init\r\n");
    err_code = ble_dists_init(&m_dists, &dists_init);
    if (err_code != NRF_SUCCESS)
    {
        NRF_LOG_ERROR("Initialization FAILED - %d\r\n", err_code);
        return err_code;
    }

    return NRF_SUCCESS;
}

uint32_t m_ranging_init(m_ble_service_handle_t * p_handle)
{
    uint32_t           err_code;

    NULL_PARAM_CHECK(p_handle);

    NRF_LOG_INFO("Ranging init\r\n");

    p_handle->ble_evt_cb = ranging_on_ble_evt;
    p_handle->init_cb    = ranging_service_init;

    err_code = ifs_tof_init(m_ifs_tof,NRF_SDH_BLE_CENTRAL_LINK_COUNT);
    APP_ERROR_CHECK(err_code);

	for (uint8_t i = 0; i < NRF_SDH_BLE_CENTRAL_LINK_COUNT; i++) { //reset all instance of m_ifs_tof and register the same handler for each node
		ifs_tof_init_struct(&m_ifs_tof[i]);
	}

    err_code = ifs_tof_enable_module();
    APP_ERROR_CHECK(err_code);

	return err_code;
}
