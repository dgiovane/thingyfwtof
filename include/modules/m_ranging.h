
#ifndef __M_RANGING_H__
#define __M_RANGING_H__

#include <stdint.h>
#include <stdbool.h>
#include "m_ble.h"

uint32_t m_ranging_init(m_ble_service_handle_t * p_handle);

#endif
